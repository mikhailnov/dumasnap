#!/bin/bash

# BTRFS snapshot automation utility
# currently works only with Ubuntu's default subvolumes layout, when the system is automatically devided into @ and @home

echoerr() { echo "$@" 1>&2; }

original_dir="$(pwd)"

# temp directory, not tmpfs (to avoid deleting files on accidental reboot)
tmp_dir="/var/tmp/dumasnap/"
rm -frv "${tmp_dir}/state/*"
mkdir -p "$tmp_dir/state/create-snapshots"
mkdir -p "$tmp_dir/state/delete-old-snapshots"

max_number_of_snapshots=5
# this variable is set for default value, but can be overwritten by the config

function var_check {
	if [[ -z "$$@" ]]; then
		echoerr "variable $@ not specified, exit 1"
		### временно отключил exit 1, нужно детаельнее проверять его работу
		#exit 1
	fi
}

function load_configs {
	# read the config file and make a list of subvolumes to snapshot
	# first, we look for the config file in the local folder to make it easier to run the development version of the script
	# in case there is no config directory in the current directory, try to use the global config and exit with error if it's not found
	#cd "$original_dir"
	if [ -d "${original_dir}/etc/dumasnap/" ]
		then
			config_dir="${original_dir}/etc/dumasnap/"
		else
			config_dir="/etc/dumasnap/"
	fi
	if [ ! -f ${config_dir}/dumasnap-snapshot-points.conf ]
		then
			echoerr "No config file with the list of snapshot points! (${config_dir}/dumasnap-snapshot-points.conf)"
			exit 1
		else
			config_file_points="${config_dir}/dumasnap-snapshot-points.conf"
			echo "Config file with subvolumes points: $config_file_points"
	fi
	
	#load config, which can overwrite some default values
	source "${config_dir}/dumasnap-general-conf.sh"
	
	cd "$tmp_dir"
	mkdir -p current_processed_subvols
	cd current_processed_subvols
	# now let's read the config file
	processed_item_count=1
	
	cat "$config_file_points" | grep -v ^# > /tmp/dumasnap_processed_file_points.list
	while read -r line
	do
		### TODO: validate config items
		# we print only columns 1 and 2 to allow writing any comments in other columns of the config line
		echo "$line" | awk '{print $1,$2}' > ${processed_item_count}.item
		processed_item_count=$((processed_item_count + 1))
	done < /tmp/dumasnap_processed_file_points.list
	# grep -v ^# removes lines which start with # (comments)
}

# delete old snapshots of the system @ subvolume
function delete_old_snapshots_system {
	number_of_available_snapshots="$(ls | wc -l)"
	var_check max_number_of_snapshots
	# check if the arguement has been specified before calling this function
	if [[ "$number_of_available_snapshots" > "$max_number_of_snapshots" ]]
		then
			# awk '{if(NR%2==0)print}' prints only not odd lines, so that we delete only 50% of snapshots, and the latest one is not deleted
			for i in $(ls -1v | grep -v dumasnap | sort -r | awk '{if(NR%2==0)print}')
			do
				for (( deleted_snapshots_counter = 0; deleted_snapshots_counter < "$number_of_available_snapshots"; deleted_snapshots_counter++ ))
				do
					btrfs subvol delete "$i"
				done
			done
	fi
}
# here we delete snapshots of subvolumes with user data; the difference is that we have to store more snapshots and make some of them unaccessible for the PC user
function delete_old_snapshots_home {
	# пока копипаста с предыдущей функции, надо дописывать
	number_of_available_snapshots="$(ls | wc -l)"
	var_check max_number_of_snapshots
	# check if the arguement has been specified before calling this function
	if [[ "$number_of_available_snapshots" > "$max_number_of_snapshots" ]]
		then
			# awk '{if(NR%2==0)print}' prints only not odd lines, so that we delete only 50% of snapshots, and the latest one is not deleted
			for i in $(ls -1v | grep -v dumasnap | sort -r | awk '{if(NR%2==0)print}')
			do
				for (( deleted_snapshots_counter = 0; deleted_snapshots_counter < "$number_of_available_snapshots"; deleted_snapshots_counter++ ))
				do
					btrfs subvol delete "$i"
				done
			done
	fi
}
function create_snapshots {
	tmp_path="/tmp/dumasnap_$(date +%s)"
	mkdir -p "$tmp_path" || echoerr "Cannot create $tmp_path"
	cd "$tmp_path" || echoerr "Cannot cd to $tmp_path"
	
	# count how many snapshots we have to create
	target_snapshots_number="$(ls -1v ${tmp_dir}/current_processed_subvols/ | wc -l)"
	real_snapshots_number=0
	
	for i in $(ls ${tmp_dir}/current_processed_subvols/)
	#for i in "${tmp_dir}/current_processed_subvols/*.item"
	do
		b="$(cat ${tmp_dir}/current_processed_subvols/$i)"
		disk_path="$(echo $b | awk '{print $1}')"
		original_subvol="$(echo $b | awk '{print $2}')"
		var_check disk_path
		var_check original_subvol
		disk_name="$(echo $disk_path | awk -F "/" '{print $(NF)}')"
		var_check disk_name
		mkdir "$disk_name" 2>/dev/null
		
		echo "Mounting disk_name : $disk_name"
		echo "Current directory is : $(pwd) "
		
		mount -v "$disk_path" "$disk_name"
		snapshot_time="$(date +%d).$(date +%m).$(date +%Y)_$(date +%H).$(date +%M)"
		btrfs subvol snapshot "${disk_name}/${original_subvol}" "${disk_name}/${original_subvol}_dumasnap_${snapshot_time}" && echo "BTRFS snapshot has been created"
		# now check, if the snapshot has really been created
		if [[ -d "${disk_name}/${original_subvol}_dumasnap_${snapshot_time}" ]]
			then
				real_snapshots_number=$((real_snapshots_number + 1))
		fi
		umount -v "$disk_name" || echoerr "Cannot unmount $disk_path !!!"
	done
	
	# these state files can be used by the systemd service
	if [[ "$real_snapshots_number" == "$target_snapshots_number" ]]
		then
			echo 0 > ${tmp_dir}/state/create-snapshots/success.state
			rm -frv "${tmp_dir}/current_processed_subvols/"
		else
			echo 1 > ${tmp_dir}/state/create-snapshots/fail.state
			mv -v "${tmp_dir}/current_processed_subvols/" "${tmp_dir}/current_processed_subvols_FAIL_$(date +%s)/"
	fi
	
}

load_configs
create_snapshots
# NOT rm -r !!!
rm -fv /tmp/dumasnap_* 2>/dev/null
