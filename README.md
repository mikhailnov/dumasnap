# dumasnap
BTRFS snapshot automation (for Ubuntu)

Currently it is not usable and is under development.

## Systemd services
However, systemd services call my fork of [apt-btrfs-snapshot](https://gitlab.com/nixtux-packaging/apt-btrfs-snapshot) to make snapshots of the `@` subvolume.

## Installing
Deb packages are available in [Tags](https://gitlab.com/mikhailnov/dumasnap/tags) and [ppa:mikhailnov/utils](https://launchpad.net/~mikhailnov/+archive/ubuntu/utils/).