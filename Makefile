#
PREFIX = /

all:
	@ echo "Nothing to compile. Use: make install, make uninstall"

install:
	install -d $(DESTDIR)/$(PREFIX)/usr/sbin
	install -m0755 ./dumasnap.sh $(DESTDIR)/$(PREFIX)/usr/sbin/dumasnap
	
	install -d $(DESTDIR)/$(PREFIX)/lib/systemd/system
	install -m0644 ./dumasnap.service $(DESTDIR)/$(PREFIX)/lib/systemd/system/dumasnap.service
	install -m0644 ./dumasnap.timer $(DESTDIR)/$(PREFIX)/lib/systemd/system/dumasnap.timer

	install -d $(DESTDIR)/$(PREFIX)/etc/dumasnap
	install -m0644 ./dumasnap-general-conf.sh $(DESTDIR)/$(PREFIX)/etc/dumasnap/dumasnap-general-conf.sh
	install -m0644 ./dumasnap-snapshot-points.conf $(DESTDIR)/$(PREFIX)/etc/dumasnap/dumasnap-snapshot-points.conf
	
uninstall:
	rm -fv $(PREFIX)/usr/sbin/dumasnap
	rm -fv $(PREFIX)/lib/systemd/system/dumasnap*
	rm -fvr $(PREFIX)/etc/dumasnap/
